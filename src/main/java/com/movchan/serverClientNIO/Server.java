package com.movchan.serverClientNIO;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Set;

public class Server {
    private static final int BUFFER_SIZE = 1024;
    private static Selector selector;
    private static final Logger LOGGER = LogManager.getLogger(Server.class);

    public void startServer(){

        LOGGER.info("Start");
        try {
            InetAddress hostAddres = InetAddress.getLocalHost();
            int port = 1024;
            LOGGER.info("Trying to accept connections");
            LOGGER.info(hostAddres.getHostAddress() + " and " + port);
            selector = Selector.open();
            ServerSocketChannel socketChannel = ServerSocketChannel.open();
            ServerSocket serverSocket = socketChannel.socket();
            InetSocketAddress address = new InetSocketAddress(hostAddres,port);
            serverSocket.bind(address);

            socketChannel.configureBlocking(false);
            int ops = socketChannel.validOps();
            socketChannel.register(selector, ops, null);

            while (true) {
                selector.select();
                Set<SelectionKey> selectionKeys = selector.selectedKeys();
                Iterator<SelectionKey> i = selectionKeys.iterator();

                while (i.hasNext()) {
                    SelectionKey key = i.next();

                    if (key.isAcceptable()) {
                        processAcceptEvent(socketChannel,key);
                    } else if (key.isReadable()) {
                        processReadEvent(key);
                    }
                    i.remove();
                }
            }
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void  processAcceptEvent(ServerSocketChannel socketChannel, SelectionKey key) throws IOException {
        LOGGER.info("Connection");

        SocketChannel myClient = socketChannel.accept();
        myClient.configureBlocking(false);
        myClient.register(selector, SelectionKey.OP_READ);
    }

    private void processReadEvent(SelectionKey key) throws IOException {
        LOGGER.info("Inside read Event");
        SocketChannel myClient = (SocketChannel) key.channel();

        ByteBuffer buffer = ByteBuffer.allocate(BUFFER_SIZE);
        myClient.read(buffer);
        String data = new String(buffer.array()).trim();
        if (data.length() > 0) {
            LOGGER.info("Message = " + data);
            if(data.equalsIgnoreCase("exit")){
                myClient.close();
                LOGGER.info("Closing Server Connection");
            }
        }
    }

    public static void main(String[] args) {
        new Server().startServer();
    }
}
