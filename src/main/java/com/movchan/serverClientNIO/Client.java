package com.movchan.serverClientNIO;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;

public class Client {
    private static final Logger LOGGER = LogManager.getLogger(Client.class);
    private static final int BUFFER_SIZE = 1024;
    private static String[] messages = {"Java is a general-purpose programming language that is class-based, object-oriented, and designed to have" +
            " as few implementation dependencies as possible. It is intended to let " +
            "application developers write once, run anywhere (WORA).",
            "exit"};

    public void runClient(){
        LOGGER.info("Starting client");
        try {
            int port = 1024;
            InetAddress hostIp = InetAddress.getLocalHost();
            InetSocketAddress socketAddress = new InetSocketAddress(hostIp,port);
            SocketChannel socketChannel = SocketChannel.open(socketAddress);

            LOGGER.info("Trying connect = " + socketAddress.getHostName()+" = " + socketAddress.getPort());

            for (String msg : messages) {
                ByteBuffer buffer = ByteBuffer.allocate(BUFFER_SIZE);
                buffer.put(msg.getBytes());
                buffer.flip();
                int bytesWritten = socketChannel.write(buffer);
                LOGGER.info("Message = " + msg + bytesWritten);
            }
            LOGGER.info("Closing client");
            socketChannel.close();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        new Client().runClient();
    }
}
