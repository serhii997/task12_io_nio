package com.movchan.view;

public interface Printable {
    void print();
}
