package com.movchan.view;

import com.movchan.model.Model;
import com.movchan.model.ModelImp;

import java.util.*;

public class MyView {
    private static Scanner scanner = new Scanner(System.in);

    private Map<String, String> menu;
    private Map<String, Printable> methodMenu;
    private ResourceBundle bundle;
    private Locale locale;
    private Model model;
    public MyView() {
        model = new ModelImp();
        locale = new Locale("en");
        bundle = ResourceBundle.getBundle("MyMenu",locale);
        setMenu();
        methodMenu = new LinkedHashMap<>();
        methodMenu.put("1", this::internationalizeMenuUkraine);
        methodMenu.put("2", this::internationalizeMenuEnglish);
        methodMenu.put("3", (() -> model.writeAndReadAircraft()));
        methodMenu.put("4",(() -> model.readFile()));
        methodMenu.put("5",(() -> model.readFileBuffer()));
        methodMenu.put("6",(() -> model.readJavaCode()));
        methodMenu.put("7",(() -> model.showMyDirectory()));
        methodMenu.put("8",(() -> model.writeAndReadBuffer()));
    }

    private void setMenu(){
        menu = new LinkedHashMap<>();
        menu.put("1",bundle.getString("1"));
        menu.put("2",bundle.getString("2"));
        menu.put("3",bundle.getString("3"));
        menu.put("4",bundle.getString("4"));
        menu.put("5",bundle.getString("5"));
        menu.put("6",bundle.getString("6"));
        menu.put("7",bundle.getString("7"));
        menu.put("8",bundle.getString("8"));
        menu.put("Q","exit");
    }
    private void internationalizeMenuUkraine(){
        locale = new Locale("uk");
        bundle = ResourceBundle.getBundle("MyMenu", locale);
        setMenu();
        printMenu();
    }

    private void internationalizeMenuEnglish(){
        locale = new Locale("en");
        bundle = ResourceBundle.getBundle("MyMenu", locale);
        setMenu();
        printMenu();
    }

    private void exit(){
        System.exit(0);
    }

    private void printMenu(){
        menu.forEach((key, value) -> System.out.println("`"+key+"` - "+"\t"+value));
    }

    public void run(){
        String input ="";
        printMenu();
        do {
            try {
                input = MyView.scanner.next().toLowerCase();
                methodMenu.get(input).print();
            } catch (NullPointerException e) {
                printMenu();
            } catch (Exception e) {
                //LOGGER.error(e.getMessage());
            }
        } while (!input.equalsIgnoreCase("Q"));
    }
}