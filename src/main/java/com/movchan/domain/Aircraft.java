package com.movchan.domain;

import java.io.Serializable;

public class Aircraft implements Serializable {
    private int speed;
    private int height;
    private int weight;
    private int price;
    private transient int freight;

    public Aircraft(int speed, int height, int weight, int price, int freight) {
        this.speed = speed;
        this.height = height;
        this.weight = weight;
        this.price = price;
        this.freight = freight;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getFreight() {
        return freight;
    }

    public void setFreight(int freight) {
        this.freight = freight;
    }

    @Override
    public String toString() {
        return "Aircraft{" +
                "speed=" + speed +
                ", height=" + height +
                ", weight=" + weight +
                ", price=" + price +
                ", freight=" + freight +
                '}';
    }
}
