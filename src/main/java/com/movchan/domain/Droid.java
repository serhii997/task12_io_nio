package com.movchan.domain;

import java.io.Serializable;

public class Droid extends Aircraft implements Serializable {
    private int battery;

    public Droid(int speed, int height, int weight, int price, int freight, int battery) {
        super(speed, height, weight, price, freight);
        this.battery = battery;
    }

    public int getBattery() {
        return battery;
    }

    public void setBattery(int battery) {
        this.battery = battery;
    }

    @Override
    public String toString() {
        return "Droid{" + super.toString()+
                "battery=" + battery +
                '}';
    }
}
