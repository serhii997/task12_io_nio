package com.movchan.domain;

import java.io.Serializable;

public class Ship extends Aircraft implements Serializable {
    private int countCrew;

    public Ship(int speed, int height, int weight, int price, int freight, int countCrew) {
        super(speed, height, weight, price, freight);
        this.countCrew = countCrew;
    }

    public int getCountCrew() {
        return countCrew;
    }

    public void setCountCrew(int countCrew) {
        this.countCrew = countCrew;
    }

    @Override
    public String toString() {
        return "Ship{" + super.toString()+
                "countCrew=" + countCrew +
                '}';
    }
}
