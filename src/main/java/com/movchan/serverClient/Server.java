package com.movchan.serverClient;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
    private final int port = 9090;
//    private final ByteBuffer byteBuffer = ByteBuffer.allocate(256);
    private BufferedReader in;
    private BufferedWriter out;
    private Socket clientSocket;
    private ServerSocket server;

    public void startServer() throws IOException {
        try {
            while (true){
            server = new ServerSocket(port);
            System.out.println("Server work");
            clientSocket = server.accept();

            in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            out = new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream()));
//            while (true) {
                String word = in.readLine();
                System.out.println("read = " + word);
                out.write("Hi, I am server");
                out.flush();
                in.close();
                out.close();
                server.close();
                if (word.equals("exit")) {
                    break;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            System.out.println("Finally");
            clientSocket.close();


        }
    }

    public static void main(String[] args) throws IOException {
        new Server().startServer();
    }
}
