package com.movchan.serverClient;

import java.io.*;
import java.net.Socket;

public class Client {
    private final int port = 9090;
    private Socket clientSocket;
    private BufferedReader reader;
    private BufferedReader in;
    private BufferedWriter out;

    void startClient() throws IOException {
        try {
            while (true){
            clientSocket = new Socket("localhost",port);
            reader = new BufferedReader(new InputStreamReader(System.in));
            in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            out = new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream()));
           // while (true){
                System.out.println("Please input smth");
                String word = reader.readLine();
                out.write(word+"\n");
                out.flush();
                String serverWord = in.readLine();
                System.out.println(serverWord);
                clientSocket.close();
                in.close();
                out.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            System.out.println("The end");
            clientSocket.close();
            in.close();
            out.close();
        }
    }

    public static void main(String[] args) throws IOException {
        new Client().startClient();
    }
}
