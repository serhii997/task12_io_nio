package com.movchan.model;

import com.movchan.domain.Aircraft;
import com.movchan.domain.Droid;
import com.movchan.domain.Ship;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ModelImp implements Model {
    private static Logger LOGGER = LogManager.getLogger(ModelImp.class);

    public ModelImp() {
    }

    @Override
    public void writeAndReadAircraft() {
        writeAircraft();
        readAircraft();
    }

    private void writeAircraft() {
        List<Aircraft> ships = new ArrayList(Arrays.asList(new Droid(200,100,20,3000,30,4000),
                new Ship(300, 11000,20000,3_000_000,2_000_000, 20)));

        try{
            ObjectOutputStream outputStream = new ObjectOutputStream(
                    new FileOutputStream("Ship.dat"));
            outputStream.writeObject(ships);
            outputStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            LOGGER.info("Save information about aircraft");
        }
    }

    private void readAircraft() {
        try {
            ObjectInputStream inputStream = new ObjectInputStream(
                    new FileInputStream("Ship.dat"));
            List<Aircraft> ships = (List<Aircraft>) inputStream.readObject();
            LOGGER.info(ships);
        }catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void readFile() {
        LOGGER.info("Read text, Please wait some time");
        int countByte = 0;
        InputStream inputStream;
        try {
            inputStream = new FileInputStream("DataBase.pdf");
            int date = inputStream.read();
            while (date != -1){
                date = inputStream.read();
                countByte++;
            }
            inputStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            LOGGER.info("count bytes = " + countByte);
        }
    }

    public void readFileBuffer() {
        LOGGER.info("Read text with Buffered reader");
        int count = 0;
        DataInputStream inputStream;
        try {
            inputStream = new DataInputStream(
                    new BufferedInputStream(
                            new FileInputStream("DataBase.pdf")));
            int date = inputStream.read();
            while (date != -1){
                date = inputStream.read();
                count++;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        LOGGER.info("count bytes = " + count);
    }

    public void readJavaCode() {
        try(BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(
                new FileInputStream("ControllerImp.java"), StandardCharsets.UTF_8))){
            int read = bufferedReader.read();
            while (read != -1){
                read = bufferedReader.read();
                if((char) read == '/'){
                    read = bufferedReader.read();
                    if(((char)read)=='/'){
                        LOGGER.info(bufferedReader.readLine());
                    }
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void showMyDirectory() {
        LOGGER.info("My Directory");
        File file = new File("D:\\Work\\task12_IO_NIO");
        if(file.exists()){
            printDirectory(file,"");
        }else {
            LOGGER.info("directory do not exist");
        }
    }

    public void writeAndReadBuffer() {
        writeChanelMethod();
        readChanelMethod();
    }

    private void readChanelMethod(){
        LOGGER.info("Write and Read file for help NIO");
        try {
            RandomAccessFile accessFile = new RandomAccessFile("text.txt","rw");
            FileChannel fileChannel = accessFile.getChannel();
            ByteBuffer buffer = ByteBuffer.allocate(48);
            int bytesRead = fileChannel.read(buffer);
            while(bytesRead != -1){
                buffer.flip();
                while (buffer.hasRemaining()){
                    LOGGER.info((char)buffer.get());
                }
                buffer.clear();
                bytesRead = fileChannel.read(buffer);
            }

            accessFile.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void writeChanelMethod(){
        System.out.println("Write text NIO");
        String str = "Java is a general-purpose programming language that is class-based, object-oriented, " +
                "and designed to have as few implementation dependencies as possible. It is intended to let " +
                "application developers write once, run anywhere (WORA)";
        try {
            RandomAccessFile accessFile = new RandomAccessFile("text.txt","rw");
            FileChannel fileChannel = accessFile.getChannel();
            ByteBuffer buffer = ByteBuffer.allocate(48);
            buffer = ByteBuffer.wrap(str.getBytes());
            fileChannel.write(buffer);
            accessFile.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void printDirectory(File file, String str){
        LOGGER.info("Directory: " + file.getName());
        str +="     ";
        File[] files = file.listFiles();
        for (File f : files) {
            if(f.isDirectory()){
                printDirectory(f,str);
            }else {
                LOGGER.info(str + "File : " +f.getName());
            }
        }
    }
}
