package com.movchan.model;

public interface Model {
    void writeAndReadAircraft();
    void readFile();
    void readFileBuffer();
    void readJavaCode();
    void showMyDirectory();
    void writeAndReadBuffer();
}
